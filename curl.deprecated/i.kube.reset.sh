# reset
	kubeadm reset -f 2> /dev/null
	dpkg -l | awk '/^rc/ {print $2}' | xargs apt-get purge -y; apt-get purge -y docker* kube* cri* container*; apt-get autoremove -y
#	apt-get purge -y docker* kube* cri* container* ; dpkg -l | grep "^rc" | awk "{print\$2}" | xargs apt-get purge -y ; apt-get autoremove -y
	rm -rf /etc/apt/keyrings/docker.gpg /etc/apt/sources.list.d/docker.list /etc/cni/net.d /etc/containerd/config.toml /etc/docker /etc/firewall* /etc/kube* /etc/sysconfig/iptables /etc/systemd/system/docker* /etc/systemd/system/multi-user.target.wants/kubelet.service /opt/container* /run/cri-dockerd.sock /usr/bin/docker-compose /usr/lib/systemd/system/container-getty@.service /usr/lib/systemd/system/kube* /usr/libexec/docker /usr/libexec/kubernetes /var/lib/container* /var/lib/containerd/io.containerd.runtime.v1.linux /var/lib/cri-dockerd /var/lib/docker* /var/lib/etcd /var/lib/kubelet /var/log/containers /var/log/pods ~/.kube ~/KUBEADM ~/.G/cri-dockerd
# install docker
	curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.docker.sh | bash
# install kube
	apt-get update
	apt-get install -y apt-transport-https ca-certificates curl golang bsdmainutils
#	curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
#	echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
	curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
	echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
	apt-get update
	apt-get install -y kubelet kubeadm kubectl
# kubeictl-whoami
#	wget $(curl https://api.github.com/repos/rajatjindal/kubectl-whoami/releases/latest |grep browser_download_url |grep linux.*amd |awk "{print\$2}" |sed "s/\"//g")
#	tar xf kubectl-whoami*
#	mv kubectl-whoami /sbin/
# install cri-dockerd
	git clone https://github.com/Mirantis/cri-dockerd.git ~/.G/cri-dockerd
	cd ~/.G/cri-dockerd
	mkdir bin
	go get && go build -o bin/cri-dockerd
#	mkdir -p /usr/local/bin
#	install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd
	install -o root -g root -m 0755 bin/cri-dockerd /usr/bin/cri-dockerd
	cp -a packaging/systemd/* /etc/systemd/system
#	sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
#	systemctl daemon-reload
#	systemctl enable cri-docker.service
#	systemctl enable --now cri-docker.socket
# fix
	rm -rf /etc/containerd/config.toml # and restart containerd
# bash completion
	kubeadm completion bash > /etc/bash_completion.d/kubeadm
	kubectl completion bash > /etc/bash_completion.d/kubectl
# systemctl
	systemctl daemon-reload
	systemctl enable kubelet docker docker.socket containerd cri-docker.service cri-docker.socket kubelet
	systemctl restart kubelet docker docker.socket containerd cri-docker.service cri-docker.socket kubelet
# reset
	kubeadm reset -f 2> /dev/null
#	apt-get clean
	dpkg -l | awk '/^rc/ {print $2}' | xargs apt-get purge -y; apt-get purge -y docker* kube* cri* container* apparmor* iptables* firewall* ; apt-get autoremove -y
	rm -rf /etc/apt/keyrings/docker.gpg /etc/apt/sources.list.d/docker.list /etc/cni/net.d /etc/containerd/config.toml /etc/docker /etc/firewall* /etc/kube* /etc/sysconfig/iptables /etc/systemd/system/docker* /etc/systemd/system/multi-user.target.wants/kubelet.service /opt/container* /run/cri-dockerd.sock /usr/bin/docker-compose /usr/lib/systemd/system/container-getty@.service /usr/lib/systemd/system/kube* /usr/libexec/docker /usr/libexec/kubernetes /var/lib/container* /var/lib/containerd/io.containerd.runtime.v1.linux /var/lib/cri-dockerd /var/lib/docker* /var/lib/etcd /var/lib/kubelet /var/log/containers /var/log/pods ~/.kube ~/KUBEADM ~/.G/cri-dockerd /etc/systemd/system/*docker* /usr/local/bin/cri-dockerd
	apt-get install -y apt-transport-https ca-certificates curl bsdmainutils # golang
# network
	cat <<EOF | tee /etc/modules-load.d/k8s.conf
	overlay
	br_netfilter
EOF
	cat <<EOF | tee /etc/sysctl.d/k8s.conf
	net.bridge.bridge-nf-call-iptables  = 1
	net.bridge.bridge-nf-call-ip6tables = 1
	net.ipv4.ip_forward                 = 1
EOF
	sysctl --system
# install golang
#	add-apt-repository ppa:longsleep/golang-backports
#	apt update
#	apt install -y golang-go
# install docker
	curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.docker.sh | bash
# install cri-dockerd
#	wget https://storage.googleapis.com/golang/getgo/installer_linux
#	chmod +x ./installer_linux
#	./installer_linux
#	source ~/.bash_profile
 	git clone https://github.com/Mirantis/cri-dockerd.git /tmp/go.build.cri-dockerd     
 	cd /tmp/go.build.cri-dockerd
#	mkdir bin
#	go build -o bin/cri-dockerd
#	mkdir -p /usr/local/bin
 	install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd
	cp -a packaging/systemd/* /etc/systemd/system
	sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
	systemctl daemon-reload
	systemctl enable cri-docker.service
	systemctl enable --now cri-docker.socket
	systemctl restart cri-docker.service
# install kube
	curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
	echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
	apt-get update
	apt-get install -y kubelet kubeadm kubectl
# bash completion
	kubeadm completion bash > /etc/bash_completion.d/kubeadm
	kubectl completion bash > /etc/bash_completion.d/kubectl
# systemctl
	systemctl daemon-reload
	systemctl enable kubelet docker docker.socket containerd cri-docker.service cri-docker.socket kubelet
	systemctl restart kubelet docker docker.socket containerd cri-docker.service cri-docker.socket kubelet
