# daemon.json
#	mkdir /etc/docker
#	cat << EOF > /etc/docker/daemon.json
#	{
#	 "exec-opts": ["native.cgroupdriver=systemd"],
#	 "log-driver": "json-file",
#	 "log-opts": {
#	 "max-size": "10m"
#	 },
#	 "storage-driver": "overlay2",
#	 "storage-opts": [
#	 "overlay2.override_kernel_check=true"
#	 ]
#	}
#EOF
# docker & docker-compose
	curl -Ls https://get.docker.com | bash
	curl -s https://api.github.com/repos/docker/compose/releases/latest |grep browser_download_url |grep linux.*$(uname -m)\" |awk "{print\$2}" |xargs wget -O /bin/docker-compose
	chmod 0755 /bin/docker-compose
# usermod
	for i in $(ls /home/) ; 
	do usermod -aG docker $i ;
	done
# systemctl
	systemctl restart docker docker.socket
	systemctl enable docker docker.socket
# cri-dockerd pre-request golang
#	wget https://storage.googleapis.com/golang/getgo/installer_linux
#	chmod +x ./installer_linux
#	./installer_linux
#	source ~/.bash_profile
# cri-dockerd
#	git clone https://github.com/Mirantis/cri-dockerd.git /tmp/go.build.cri-dockerd
#	cd /tmp/go.build.cri-dockerd
#	mkdir bin
#	go build -o bin/cri-dockerd
#	mkdir -p /usr/local/bin
#	install -o root -g root -m 0755 bin/cri-dockerd /usr/local/bin/cri-dockerd
#	cp -a packaging/systemd/* /etc/systemd/system
#	sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
#	systemctl daemon-reload
#	systemctl enable cri-docker.service
#	systemctl enable --now cri-docker.socket
#	systemctl restart cri-docker.service
