helm install metallb bitnami/metallb -n kube-system
kubectl get  cm/metallb-config -n kube-system -o yaml > /tmp/.metallb
cat << EOF >> /tmp/.metallb
data:
  config: |
    address-pools:
    - name: my-ip-space
      protocol: layer2
      addresses:
      - 192.168.50.141-192.168.50.145
#     - $RANGE
EOF
kubectl apply -f /tmp/.metallb
# export RANGE=192.168.50.141-192.168.50.145
# envsubst < tmp/.metallb
# 
# echo export\ RANGE=192.168.50.141-192.168.50.145
# echo envsubst\ <\ /tmp/.metallb
