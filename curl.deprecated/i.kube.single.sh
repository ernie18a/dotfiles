# init
#	kubeadm config images pull
	kubeadm init --cri-socket unix:///var/run/cri-dockerd.sock 2>&1 |tee /tmp/KUBE.INIT
# .kube
	mkdir -p $HOME/.kube
	cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
	chown $(id -u):$(id -g) $HOME/.kube/config
# taint
	kubectl taint nodes --all node-role.kubernetes.io/master-
	kubectl taint nodes --all node-role.kubernetes.io/control-plane-
# helm
	curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.helm.sh | bash
# cilium
	helm install cilium cilium/cilium -n kube-system
# openebs
	kubectl apply -f https://openebs.github.io/charts/openebs-operator-lite.yaml -f https://openebs.github.io/charts/openebs-lite-sc.yaml
	kubectl delete --force --grace-period=0 sc openebs-device
	kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}' # set default sc even if its the only one
# nfs deprecated
#	apt-get install -y nfs-kernel-server
#	echo ' /NFS *(rw,no_root_squash,async) ' > /etc/exports
#	mkdir /NFS
#	chmod 0777 /NFS
#	systemctl restart nfs-server.service
#	helm install nfs-sc nfs-subdir-external-provisioner/nfs-subdir-external-provisioner -n kube-system --set nfs.server=$(env |grep SSH_CONNECTION | awk "{print\$3}") --set nfs.path=/NFS
#	kubectl patch storageclass nfs-client -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
# metrics
	curl -fsSL https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml > /tmp/a.yaml
	sed -i '/- args:/a \ \ \ \ \ \ \ \ \- --kubelet-insecure-tls' /tmp/a.yaml
	kubectl apply -f /tmp/a.yaml
# krew
#	(
#	  set -x; cd "$(mktemp -d)" &&
#	  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
#	  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
#	  KREW="krew-${OS}_${ARCH}" &&
#	  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
#	  tar zxvf "${KREW}.tar.gz" &&
#	  ./"${KREW}" install krew
#	)

# kustomize
 	curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"| bash
 	mv kustomize /bin/
 	kustomize completion bash > /etc/bash_completion.d/kustomize
