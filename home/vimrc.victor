"快捷鍵設定
" Vim 預設設定
" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif


" Vim set 設定
set backspace=indent,eol,start "在這三個特殊位置也能進行刪除動作
set nobackup		" keep a backup file
set viminfo='20,\"50	" read/write a .viminfo file, don't store more, than 50 lines of registers
set nocompatible	" 關閉 Vi 兼容模式
set hlsearch            " 開啟高亮搜索模式
set incsearch		" 開啟增量搜索模式
set autoindent		" 開啟自動縮排
set cindent
set smartindent
set cursorline		" 光標底線
set shiftwidth=4	" 自動縮排對齊間隔數,向右或向左一個縮排的寬度 
set history=50		" 保留 50 個使用過的指令
set ruler		" 顯示所在位置的行號和列號
set showcmd		" 在螢幕右下角顯示未完成的指令輸入
set showmode		" 在螢幕左下角顯示當前的模式名稱
set scrolloff=8		" 螢幕滾動時,邊邊保留至少 5 行的高度
set display=truncate	" Show @@@ in the last line if it is truncated.
set wildmenu		" Tab 自動補全命令參數
set nu rnu		" 顯示絕對行數和相對行數
set nrformats-=octal    " 不識別八進制數
set paste		" 複製貼上格式不會跑掉
set ttimeout		" time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key
set cursorcolumn
hi search none
filetype plugin indent on

"color setting
if (empty($TMUX))
  if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  if (has("termguicolors"))
    set termguicolors
  endif
endif

syntax on
set bg=dark             " 設定背景上色 light or dark
set t_Co=256            " 讓 Vim 支援 256 色
colorscheme onedark 	" 套用colorscheme
let g:onedark_termcolors=16

"yaml setting
autocmd FileType yaml setlocal ai ts=2 sts=2 sw=2 et 






" Only do this part when compiled with support for autocommands
if has("autocmd")
  augroup redhat
  autocmd!
  " In text files, always limit the width of text to 78 characters
  " autocmd BufRead *.txt set tw=78
  " When editing a file, always jump to the last cursor position
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal! g'\"" |
  \ endif
  " don't write swapfile on most commonly used directories for NFS mounts or USB sticks
  autocmd BufNewFile,BufReadPre /media/*,/run/media/*,/mnt/* set directory=~/tmp,/var/tmp,/tmp
  " start with spec file template
  " 1724126 - do not open new file with .spec suffix with spec file template
  " apparently there are other file types with .spec suffix, so disable the
  " template
  " autocmd BufNewFile *.spec 0r /usr/share/vim/vimfiles/template.spec
  augroup END
endif

if has("cscope") && filereadable("/usr/bin/cscope")
   set csprg=/usr/bin/cscope
   set csto=0
   set cst
   set nocsverb
   " add any database in current directory
   if filereadable("cscope.out")
      cs add $PWD/cscope.out
   " else add database pointed to by environment
   elseif $CSCOPE_DB != ""
      cs add $CSCOPE_DB
   endif
   set csverb
endif

if &term=="xterm"
     set t_Co=8
     set t_Sb=[4%dm
     set t_Sf=[3%dm
endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif

" Don't wake up system with blinking cursor:
let &guicursor = &guicursor . ",a:blinkon0"

" Source a global configuration file if available
if filereadable("/etc/vimrc.local")
  source /etc/vimrc.local
endif

