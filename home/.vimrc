":imap <special> jk <Esc>
"Plugin 'Yggdroot/indentLine' 
"autocmd BufRead,BufNewFile *.tf set filetype=json
"autocmd BufRead,BufNewFile *.tfstate *.tf set filetype=json
"colorscheme slate
"let g:indentLine_bgcolor_gui = '#FF5F00'
"let g:indentLine_bgcolor_term = 202
"set cursorcolumn
"set cursorline
"set mouse=a
"set mouse=nicr
"set mouse=r
"set nopaste
"set syntax=sh
filetype indent on
filetype on
filetype plugin on
hi CursorColumn ctermbg=8
set ambw=double
set autoindent 
set cursorcolumn
set encoding=utf-8  " The encoding displayed.
set fileencoding=utf-8  " The encoding written to file.
set hlsearch incsearch
set mouse=
set nocompatible
set noswapfile
set paste
set ruler
set t_u7= " https://superuser.com/questions/1284561/why-is-vim-starting-in-replace-mode
set tabstop=4
set ttymouse=
set wildmenu
syntax enable
syntax on
syntax sync fromstart
