[[_TOC_]]
# ~/.bash_profile使用方式
```bash
source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"
```
- 用客製指令執行`~/.bash_profile`中兩百多個alias和bash function
# wsl
- 在wsl中透過變數快速移動到windows`桌面`或者`下載`
```bash
export WIN_USER=`ls /mnt/c/Users 2>/dev/null |grep -v "All\|Default\|desktop.ini\|Public\|USER\|Administrator"` 
export WIN_USER_DIR=/mnt/c/Users/$WIN_USER
alias CDW=' cd $WIN_USER_DIR '
alias CDWE=' cd $WIN_USER_DIR/Desktop ; ls -l --color '
alias CDWO=' cd $WIN_USER_DIR/Downloads ; ls -l --color '
```
# git
- bash prompt
```bash
which git &>/dev/null  && export PS1="[\\h \w \$(git rev-parse --abbrev-ref HEAD 2>/dev/null)] \\$ " || export PS1="[\\h \w] \\$ "
```
## 筆記管理
#### 編輯筆記再推到remote branch
```bash
alias VNP=' cd ~/.G/misc/notes ; git pull ; vim ./commands.txt ; git rm -r --cached . > /dev/null ; git add -A ; git commit -amcommand.txt\ update &> /dev/null ; git push ; cd - &>/dev/null '
alias VEP=' cd ~/.G/misc/notes ; git pull ; vim ./eng.txt ; git rm -r --cached . > /dev/null ; git add -A ; git commit -ameng.txt\ update &> /dev/null ; git push ; cd - &>/dev/null '
```
#### 用CLI查筆記
```bash
CNG() { cat ~/.G/misc/notes/commands.txt 2>/dev/null |grep -i --color $1 || curl -fsSL https://gitlab.com/ernie18a/misc/-/raw/main/notes/commands.txt |grep -i --color $1 ; }
CEG() { cat ~/.G/misc/notes/eng.txt 2>/dev/null |grep -i --color $1 || curl -fsSL https://gitlab.com/ernie18a/misc/-/raw/main/notes/eng.txt |grep -i --color $1 ; }
```
- 想 vacuum journalctl時，可以用易辨識的字串查詢
```bash
[C23011-ErnieHo ~ ] $ CNG vac
sudo journalctl --vacuum-size=00.1K
[C23011-ErnieHo ~ ] $
```
- 或者用關鍵字journal查詢
```bash
[C23011-ErnieHo ~ ] $ CNG journal
journalctl -S "5 minutes ago"
cat journalctl.1213.log |grep -E bondSERVICE\|bondSTORAGE\|ixgbe\|ens6f[0,1]\|eno1
sudo journalctl --vacuum-size=00.1K
[C23011-ErnieHo ~ ] $
```
## 讓repo恢復成remote branch的狀態
```bash
alias GRC=' git fetch --prune ; git reset --hard && git clean -f -d ; git rm -r --cached . > /dev/null ; git reset --hard ; git pull '
```
## 讓`~/.G/`底下的repo恢復成remote branch的狀態
```bash
GRCA() { cd ~/.G ; for REPO in `ls`; do (cd "$REPO"; git fetch --prune ; git reset --hard && git clean -f -d ; git rm -r . --cached . > /dev/null ; git reset --hard ; git pull ); done; cd - ; }
```
## 把修改過的repo推到remote branch
```bash
PUSHM() { git rm -r --cached . > /dev/null ; git add -A ; git commit -m "$@" &>/dev/null ; git push;}
```
- `git rm`→`git add`→`git commit`→`git push`
```bash
[C23011-ErnieHo ~/.G/dotfiles main] $ PUSHM add\ note\ to\ README.md
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 16 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 546 bytes | 546.00 KiB/s, done.
Total 4 (delta 3), reused 0 (delta 0), pack-reused 0
To gitlab.com:ernie18a/dotfiles.git
   976364d..b9f863c  main -> main
[C23011-ErnieHo ~/.G/dotfiles main] $ 
```
## 編輯`./README.md`再推到remote branch
- 編輯當下目錄的`README.md
- 推到remote branch
```bash
alias VRP=' git pull && vim README.md && git rm -r --cached . > /dev/null && git add -A && git commit -amREADME.md\ update &> /dev/null && git push '
```
# docker
## 顯示主機上被container volume的路徑或檔案
```bash
alias DPL=' for i in $(docker ps |grep -v STATUS |awk "{print\$1}") ; do docker inspect $i |grep -v \/var\/lib |grep Source ; done '
```
- stdout顯示被volume的檔案在`/root/haproxy/haproxy.cfg`
```bash
[r138 ~ ] # DPL
"Source": "/root/haproxy/haproxy.cfg",
[r138 ~ ] # docker ps 
CONTAINER ID  IMAGE COMMAND   CREATED STATUS PORTS   NAMES
669cd6c287f6  localhost/haproxy:latest  haproxy -f /usr/l...  5 days ago  Up 5 days ago  172.16.169.130:80->80/tcp, 172.16.169.130:443->443/tcp  e-car-haproxy
[r138 ~ ] # 
```
## tag和push
```bash
DOCKERTP() { docker tag $1 ernie18a/$2:$3 ; docker push ernie18a/$2:$3 ; }
```
- 把 nginx:alpine-slim tag和push到私人 repostory ernie18a/nginx:20231218
```bash
[ampere ~ ] $ docker images | grep nginx 
nginx   alpine-slim   436153bee7bf   2 weeks ago 12.5MB
[ampere ~ ] $ DOCKERTP nginx:alpine-slim nginx 20231218
The push refers to repository [docker.io/ernie18a/nginx]
58f372fec67d: Mounted from library/nginx
ac989bf99e40: Mounted from library/nginx
0caf0d836bb2: Mounted from library/nginx
722dc0e320a2: Mounted from library/nginx
5a2c43b488a5: Mounted from library/nginx
51901e51e275: Mounted from library/nginx
0faf9b67be60: Mounted from library/nginx
20231218: digest: sha256:010e53362a161db3a51bde36b42de760cf050542b8a639cb6c7dde891f65a564 size: 1776
[ampere ~ ] $ 
```
# k8s
## 測 k8s loadbalancer
```bash
alias KCX=' kubectl run --image nginx:alpine-slim nginx-ernie-test --port 80 ; kubectl expose pod nginx-ernie-test --type LoadBalancer --name nginx-ernie-test --port 80 ; kubectl get svc '
alias KDX=' kubectl delete po nginx-ernie-test ; kubectl delete svc nginx-ernie-test '
```
## 無視namespace，直接顯示k8s component訊息
```bash
kwccn() { kubectl $1 $2 $3 -n $(kubectl get $2 -A | grep $3 | awk "{print\$1}") ; }
KLPN() { kubectl logs $1 -n $(kubectl get po -A | grep $1 | awk "{print\$1}") ; }
```
- 範例
```bash
[ampere ~ ] $ kubectl get po -A | grep -E NAMESPACE\|cilium-nw4lj\|coredns-5dd5756b68-lff6p
NAMESPACE NAME   READY   STATUSRESTARTS   AGE
kube-system   cilium-nw4lj   1/1 Running   12 85d
kube-system   coredns-5dd5756b68-lff6p   1/1 Running   0  26d
[ampere ~ ] $ KLPN cilium-nw4lj | wc
Defaulted container "cilium-agent" out of: cilium-agent, config (init), mount-cgroup (init), apply-sysctl-overwrites (init), mount-bpf-fs (init), clean-cilium-state (init), install-cni-binaries (init)
6504524   78649
[ampere ~ ] $ kwccn describe po coredns-5dd5756b68-lff6p | wc 
 64 1732564
[ampere ~ ] $ 
```
# 操作符合字元的檔案/目錄
- 把`ls | grep ...`和後續的動作串再一起
## `cd`到符合字元的目錄
```bash
lgcd() { cd $(ls -Ah |grep -i $1 ) || ls -Ah | grep -i $i ; }
```
## `vim`符合字元的檔案
```bash
lgv() { vim $(ls -Ah |grep -i $1 ) || ls -Ah | grep -i $i ; }
```
## `cat`符合字元的檔案
```bash
lgc() { cat $(ls -Ah |grep -i $1 ) || ls -Ah | grep -i $i ; }
```
- 以`lgc為例`
```bash
[C23011-ErnieHo /tmp/dotfiles/curl ] $ ll *auth* 
-rw-r--r-- 1 e e 720 Dec 18 16:48 e.authorized_keys.ed25519.sh
-rw-r--r-- 1 e e 962 Dec 18 16:48 e.authorized_keys.sh
[C23011-ErnieHo /tmp/dotfiles/curl ] $
[C23011-ErnieHo /tmp/dotfiles/curl ] $ lgc 255 | head -n 2
mkdir ~/.ssh 2>/dev/null
chmod 0700 ~/.ssh
[C23011-ErnieHo /tmp/dotfiles/curl ] $
```
# systemctl
```bash
alias SYSD=' sudo systemctl disable '
alias SYSE=' sudo systemctl enable '
alias SYSR=' sudo systemctl restart '
alias SYSS=' sudo systemctl stop '
alias LUF=' systemctl list-unit-files |grep -i '
alias SYS=' systemctl status '
```
# 影音相關
## ffmpeg轉檔
```bash
FFMPEG() { for i in *.$1 ; do ffmpeg -y -i "$i" "$(echo "$i"|cut -d\. -f1).$2" ; done ; }
```
## yt-dlp youtube大量下載
- 下載成影片
```bash
YTDLV() { while read -r i; do yt-dlp -c -r 30k -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' "$i"; done < "$1"; }
```
- 下載成語音
```bash
YTDLA() { while read i ; do yt-dlp -c -r 30k --prefer-ffmpeg --extract-audio --audio-format mp3 --no-check-certificate "$i" ; done <$1 ; }
```
# 其他功能
## find優化
- 一個是從`當下`目錄開始找，另一個是從`根`目錄開始找
```bash
alias FINDD=' find . -iname 2>/dev/null '
alias FIND=' find / -iname 2>/dev/null '
```
## 遞迴找字串
```
alias GRI=' grep -Ri --color 2>/dev/null ' # -i
```
## 從stdout找IP
```bash
alias GIP=' grep -iE --color "([0-9]{1,3}[\.]){3}[0-9]{1,3}" '
```
## tmux客製化
- 如果沒有tmux session就建立另一個tmux
```bash
alias tmuxad=' tmux a -d || tmux '
```
## watch客製化
- 後面接指令，不用`\ `取代空格
```bash
WATCH() { watch "$@" ; }
```
## awk客製化
- 後面接原本要放在 '{print `$3`}'的數字
- `$2`可放其他flag像是`-F`
```bash
AWK() { awk $2 "{print\$$1}" ; }
```
## sed編輯檔案內的字串
- 一個是刪除，另一個是取代
```bash
SEDID() { sed -i "/$1/d" $2 ; }
SEDIS() { sed -i "s/$1/$2/g" $3 ; }
```
## 提升stdout的可讀性
- 移除無效的字串或者沒意義的line
```bash
GV() { grep -v ^\ *$ |grep -v ^\ *# |grep -v ^\t*$ |grep -v ^\t*# |grep -v ^$'\r' | grep -vP ^'\t*#' ; }
```
## 初始化VM
```bash
NTPL() { growpart /dev/sda 2 ; resize2fs /dev/sda2 ; hostnamectl set-hostname u$(echo $1 |awk -F. "{print\$4}") ; sed -i "s/\(- \).*\(\/\)/\1$1\2/g" /etc/netplan/00-installer-config.yaml ; }
```
- 調整partition
- 用`$1`設定hostname和IP
## 建立指令的bash completion
- 建立指令`$1`的bash completion
```bash
CMPL() { $1 completion bash > /etc/bash_completion.d/$1 || ls -l /etc/bash_completion.d ; }
```
## 顯示使用中的IP
- 目前只能測class C
```bash
PING() { for i in $(seq 0 255) ; do ping $1.$i -c1 -W1 & done |grep from |awk '{print $4}' |awk -F: '{print $1}' | sort -V ; }
```
## 讀、寫快速筆記
- 筆記命名原則：`~/.T`~`~/.T999`
```bash
CT() { cat ~/.T$1 2>/dev/null ; }
VT() { vim ~/.T$1 ; }
```
## 編輯`~/.bash_profile`再推到remote branch
```bash
alias VBP=' cd ~/.G/dotfiles ; git pull >/dev/null ; vim ~/.G/dotfiles/home/.bash_profile ; git rm -r --cached . >/dev/null ; git add -A ; git commit -am.bash_prifile\ update &> /dev/null ; git push 1>/dev/null ; cd - &>/dev/null '
```
## 查詢既有bash組合(alias)或bash function
```bash
CBG() { curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile |grep -i --color $1 2>/dev/null || cat ~/.bash_profile 2>/dev/null |grep -i --color $1 ; }  
```
## 暫時使用私人`~/.bash_profile`
```bash
alias CDZ=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.danger.zone.sh | bash '
```
