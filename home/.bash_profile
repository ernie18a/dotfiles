
	cd
	complete -C '/usr/local/bin/aws_completer' aws &>/dev/null # complete -C /usr/bin/terraform terraform
	complete -C /usr/bin/terraform terraform
	source /System/Volumes/Data/private/etc/bash_completion.d/* 2>/dev/null
	source /etc/bash_completion.d/* 2>/dev/null
	source /usr/share/bash-completion/bash_completion 2>/dev/null
	export DEBIAN_FRONTEND=noninteractive
	export EDITOR=vim
	export GIT_EDITOR=vim
	export HISTCONTROL=ignoredups:erasedups &>/dev/null
	export HISTFILESIZE= &>/dev/null
	export HISTSIZE= &>/dev/null
	export KUBE_EDITOR=/bin/vim &>/dev/null
	export MAILCHECK=0 &>/dev/null
	export PATH="$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.krew/bin:$HOME/go/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/snap/bin:/opt/homebrew/bin:/Users/ernieho/Library/Python/3.9/bin:$HOME/.docker/cli-plugins"
export PATH="/opt/homebrew/opt/mysql-client/bin:$PATH"
	export WIN_USER=`ls /mnt/c/Users 2>/dev/null |grep -iv "All\|Default\|desktop.ini\|Public\|USER\|Administrator\|super"`
	export WIN_USER_DIR=/mnt/c/Users/$WIN_USER
	which git &>/dev/null  && export PS1="[ \\h \w \$(git rev-parse --abbrev-ref HEAD 2>/dev/null)] \\$ " || export PS1="[ \\h \w ] \\$ "
	PROMPT_COMMAND=' history -a '
	source ~/.IPS 2>/dev/null
	touch ~/.hushlogin
	alias CAL=' cal  $(date +%Y) '
	alias APT=' bash -c "curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.apt.sh" | bash -s -- '
	alias TFA=' [ -f maina.tf ] && rm .* ; vim main.tf ; terraform init ; terraform apply -auto-approve ' # terraform plan
	alias bcl=' bat -ppl '
	alias bcb=' bat -ppl bash '
	alias bcjs=' bat -ppl js '
	alias bcj=' bat -ppl json '
	alias bcm=' bat -ppl md '
	alias MD=' bat -ppl md *.md '
	alias BR=' bat -ppl md README.md '
	alias bcmr=' bat -ppl md README.md '
	alias bcp=' bat -ppl py '
	alias bcr=' bat -ppl rb '
	alias bct=' bat -ppl tf '
	alias bcy=' bat -ppl yml '
	alias caat=' cat '
	alias car=' cat '
	alias cart=' cat '
	alias catr=' cat '
	alias caty=' cat '
	alias cay=' cat '
	alias cayt=' cat '
	alias CDC=' cat docker-compose.yml '
	alias CSG=' cat ~/.ssh/config | grep -i --color '
	alias CDW=' cd $WIN_USER_DIR '
	alias CDWE=' cd $WIN_USER_DIR/Desktop ; ls -l --color '
	alias CDWO=' cd $WIN_USER_DIR/Downloads ; ls -l --color '
	alias ..=' cd ../ '
	alias ...=' cd ../../ '
	alias ....=' cd ../../../ '
	alias .....=' cd ../../../../ '
	alias ......=' cd ../../../../../ '
	alias CDI=' cd ~/.G/15 ; ls -l '
	alias CDM=' cd ~/.G/1v9/migrate ; ls -A --color |sort '
	alias VBP=' cd ~/.G/dotfiles ; git pull >/dev/null ; vim ~/.G/dotfiles/home/.bash_profile ; git rm -r --cached . >/dev/null ; git add -A ; git commit -am.bash_prifile\ update &> /dev/null ; git push 1>/dev/null ; cd - &>/dev/null '
	alias CDGC=' cd ~/.G/dotfiles/curl ; ls -A --color |sort '
	alias CDME=' cd ~/.G/misc/examples ; ls -A --color |sort '
	alias VNP=' cd ~/.G/misc/notes ; git pull ; vim ./commands.txt ; git rm -r --cached . > /dev/null ; git add -A ; git commit -amcommand.txt\ update &> /dev/null ; git push ; cd - &>/dev/null '
	alias VEP=' cd ~/.G/misc/notes ; git pull ; vim ./eng.txt ; git rm -r --cached . > /dev/null ; git add -A ; git commit -ameng.txt\ update &> /dev/null ; git push ; cd - &>/dev/null '
	alias co=' cp '
	alias cp=' cp -rf '
	alias BNA=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.brc.n.auth.sh | bash '
	alias CDZ=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.danger.zone.sh | bash '
	alias ED2=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.ed25519.sh | bash '
	alias NBP=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.new_bash_profile.sh | bash '
	alias VIMRC=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.vimrc.sh | bash '
	alias AWS=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.aws-cli.sh | bash '
	alias BAT=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.bat.sh | bash '
	alias GOLANG=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.golang.sh | bash '
	alias HELM=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.helm.sh | bash '
	alias CKR=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.reset.sh |bash '
	alias SINGLE=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.single.sh | bash '
	alias REPO=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.rhel.repo.mount.sh | bash '
	alias RUNNER=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.runner.sh | bash '
	alias PUB=' curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.pub.sh | bash '
	alias DOCKER=' curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.docker.sh | bash '
	alias IPP=' curl ipinfo.io ; curl ifconfig.io '
	alias DCR2=' docker-compose down ; docker-compose up -d '
	alias DU=' du -sh . ; du -hd1 2>/dev/null |grep "G\|M" '
	alias DS=' echo "--no-stream" ; sudo docker stats '
	alias FINDD=' find . -iname 2>/dev/null '
	alias FIND=' find / -iname 2>/dev/null '
	alias DPS=' for i in $(sudo docker ps |grep -v STATUS |awk "{print\$1}") ; do sudo docker inspect $i |grep -v \/var\/lib |grep Source ; done '
	alias freeh=' free -h '
	alias GCO=' git checkout '
	alias GC=' git clone --depth=1 -q '
	alias GRC=' git fetch --prune && git reset --hard && git clean -f && git pull ' # git rm -r --cached
	alias VRP=' git pull && vim README.md && git rm -r --cached . > /dev/null && git add -A && git commit -amREADME.md\ update &> /dev/null && git push '
	alias GSN=' git start next '
	alias GS=' git status '
	alias VER=' git verify '
	alias GE=' grep -Ei --color '
	alias GV=' grep -Ev "^\s*(#|$)" '
	alias GR=' grep -R --color 2>/dev/null '
	alias G=' grep -i --color '
	alias GIP=' grep -iE --color "([0-9]{1,3}[\.]){3}[0-9]{1,3}" '
	alias GRI=' grep -ri --color 2>/dev/null ' # -i
	alias V=' grep -v '
	alias VE=' grep -vE '
	alias GURL=' grep -vE 127.0.0\|log.*log\|index\|1.2.3 | grep -E "([a-zA-Z0-9]+\.[a-zA-Z0-9]+\.[a-zA-Z0-9]+)" ' # -r & -o removed
	alias cpum=' grep processor /proc/cpuinfo |wc |awk "{print\$1}" ; free -h |grep Mem: |awk "{print\$2}" '
	alias LTH2=' gsed -i "/PUSHM\|rere\|nmap\|nc\|.bash_history\|help\|CNG\|CBG\|reboot\|poweroff\|HISTFILE\|help\|mtr\|gping\|BRC\|wget\|NC\|ping\|curl\|HG\|HH\|LTH\|STEALTH\|source\|SAS\|export\|history\|rm\|ernie18a\|sshrc\|rsync\|scp\|scpr/d" ~/.bash_history ; rm -rf /tmp/*sshrc* '
	alias HSR=' helm search repo '
	alias HU=' helm uninstall '
	alias IPA=' ip a |grep scope\ global |grep -vE docker\|br\- |grep -iE --color "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | awk "{print\$2}"| awk -F/ "{print\$1}" '
	alias IPT=' iptables -t nat -L -n -v '
	alias IPTG=' iptables -t nat -L -n -v | grep -i --color '
	alias IPT2=' iptables-save '
	alias KARG=' kubectl api-resources |grep -i --color '
	alias KARF=' kubectl apply --recursive -f . '
	alias KAF=' kubectl apply -f '
	alias KDEL=' kubectl delete '
	alias KDRF=' kubectl delete --recursive -f . &>/dev/null '
	alias KDF=' kubectl delete -f '
	alias KDX=' kubectl delete po nginx-ernie-test ; kubectl delete svc nginx-ernie-test '
	alias KDS=' kubectl delete pod -A --field-selector=status.phase==Succeeded '
	alias KD=' kubectl describe '
	alias KE=' kubectl edit '
	alias KG=' kubectl get '
	alias KGA=' kubectl get -A '
	alias KGAPV=' kubectl get -A pv ; kubectl get -A pvc '
	alias KGAAG=' kubectl get all -A |grep -i --color '
	alias KGE=' kubectl get event -A -o custom-columns="LAST SEEN:{lastTimestamp},NAME:{metadata.name},KIND:{involvedObject.kind},TYPE:{type},MESSAGE:{message}" |sort -V '
	alias KGEVN=' kubectl get event -A -o custom-columns="LAST SEEN:{lastTimestamp},NAME:{metadata.name},TYPE:{type},MESSAGE:{message}" |grep -v Normal '
	alias KP=' kubectl get po -A -o custom-columns=NAME:.metadata.name,READY:.status.containerStatuses[0].ready,STATUS:.status.phase | grep -v true.*Running ' # |grep -vE Running\|Complete
	alias KGAP=' kubectl get pod -A '
	alias KGAPG=' kubectl get pod -A |grep -i --color '
	alias KIL=' kubectl get pods -A -o jsonpath="{.items[*].spec.containers[*].image}" |tr -s "[[:space:]]" "\n" |sort |uniq '
	alias KDELP=' kubectl get pvc |AWK 1 |xargs kubectl delete pvc ; KG pv |AWK 1 |xargs kubectl delete pv '
	alias KGAS=' kubectl get svc -A -o custom-columns="NAMESPACE:{metadata.namespace},NAME:{metadata.name},TYPE:{spec.type},CLUSTER-IP:{spec.clusterIP},EXTERNAL-IP:{.status.loadBalancer.ingress[*].ip},PORD:{.spec.ports[*].port}" '
	alias KGASG=' kubectl get svc -A -o custom-columns="NAMESPACE:{metadata.namespace},NAME:{metadata.name},TYPE:{spec.type},CLUSTER-IP:{spec.clusterIP},EXTERNAL-IP:{.status.loadBalancer.ingress[*].ip},PORD:{.spec.ports[*].port}" |grep -i --color '
	alias KL=' kubectl logs '
	alias KCX=' kubectl run --image nginx:alpine-slim nginx-ernie-test --port 80 ; kubectl expose pod nginx-ernie-test --type LoadBalancer --name nginx-ernie-test --port 80 ; kubectl get svc '
	alias lld=' ls --color -Ald */ .*/ '
	alias l=' ls --color -ld .?* '
	alias lg=' ls -Ah |grep -i --color '
	alias lll=' ls -Alh --color '
	alias kk=' ls -l --color '
	alias ll=' ls -l --color '
	alias llh=' ls -lh --color '
	alias CDB=' mkdir ~/.BACKUP &>/dev/null ; cd ~/.BACKUP ; ls -A --color |sort '
	alias CDG=' mkdir ~/.G &>/dev/null ; cd ~/.G/* 2>/dev/null || { cd ~/.G && ls -A --color |sort ; } '
	alias NC=' nc -zvw2 '
	alias PS5=' ps aux | sort -nrk 3,3 | head -n 5 '
	alias rm=' rm -rf '
	alias TPLT=' rm ~/.ssh/known_hosts ; echo 172.16.169.2 ; ssh -o ConnectTimeout=2 root@172.16.169.251 2>/dev/null '
	alias RSYNC=' rsync -ruzvP '
	alias scpr=' scp -rC '
	alias LTH=' sed -i "/PUSHM\|rere\|nmap\|nc\|.bash_history\|help\|CNG\|CBG\|reboot\|poweroff\|HISTFILE\|help\|mtr\|gping\|BRC\|wget\|NC\|ping\|curl\|HG\|HH\|LTH\|STEALTH\|source\|SAS\|export\|history\|rm\|ernie18a\|sshrc\|rsync\|scp\|scpr/d" ~/.bash_history ; rm -rf /tmp/*sshrc* '
	alias SHK=' sort -hk '
	alias SB=' source ~/.BRC || source ~/.G/dotfiles/home/.bash_profile 2>/dev/null || source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)" ; cd - &>/dev/null '
	alias SS=' ss -lutn '
	alias SSG=' ss -lutn |grep -i --color '
	alias GLANCES=' sudo apt-get install -y software-properties-common ; sudo add-apt-repository -y ppa:deadsnakes/ppa ; sudo apt-get update -y ; sudo apt-get install -y python3-pip ; sudo pip3 install pip glances -U '
	alias AGP=' sudo apt-get purge -y ; dpkg -l | grep "^rc" | awk "{print\$2}" | xargs sudo apt-get purge -y ; sudo apt-get autoremove -y '
	alias AGU=' sudo apt-get purge -y >/dev/null '
	alias AGG=' sudo apt-get update >/dev/null ; apt list |tee ~/.APT |grep -i --color '
	alias AGI=' sudo apt-get update >/dev/null ; sudo apt-get install -y >/dev/null '
	alias DEI=' sudo docker exec -it '
	alias DIL=' sudo docker image list '
	alias DIR=' sudo docker image rm '
	alias DL=' sudo docker logs '
	alias DP=' sudo docker ps -a --format "table {{.Names}}\t{{.Status}}\t{{.Ports}} " '
	alias DPG=' sudo docker ps -a --format "table {{.Names}}\t{{.Status}}\t{{.Ports}} " |grep --color -i '
	alias DRFA=' sudo docker rm -f $(sudo docker ps -a -q) '
	alias DRF=' sudo docker rm -f '
	alias PRUNE=' sudo docker system prune -f --volumes 2>/dev/null ; docker volume prune -af 2>/dev/null '
	alias PRUNEI=' sudo docker system prune -fa 2>/dev/null ; docker volume prune -af 2>/dev/null '
	alias DCD=' sudo docker-compose down '
	alias DCR=' sudo docker-compose down ; sudo docker-compose up -d '
	alias llll=' sudo find . '
	alias llllg=' sudo find . |grep -i '
	alias PIU=' sudo pip3 install -U ' # --system
	alias tmprm=' sudo rm -rf /tmp/{,.}* 2>/dev/null ; cd /tmp '
	alias SU=' sudo su - '
	alias SYSD=' sudo systemctl disable '
	alias SYSE=' sudo systemctl enable '
	alias SYSR=' sudo systemctl restart '
	alias SYSS=' sudo systemctl stop '
	alias SVH=' sudo vim /mnt/c/Windows/System32/drivers/etc/hosts '
	alias YI=' sudo yum install -y >/dev/null '
	alias LIST=' sudo yum list >~/.YUM &>/dev/null || apt-get update 1>/dev/null ; apt-get list > ~/.APT 1>/dev/null '
	alias YG=' sudo yum list |tee ~/.YUM |grep -i --color '
	alias YU=' sudo yum remove -y >/dev/null '
	alias LUF=' systemctl list-unit-files |grep -i '
	alias SYS=' systemctl status '
	alias ZCF=' tar -zcf '
	alias CF=' tar cf '
	alias XF=' tar xf '
	alias tmuxad=' tmux a -d 2>/dev/null || tmux '
	alias TC=' tree -Ca '
	alias TCG=' tree -Ca |grep -i --color $1'
	alias TCL=' tree -CaL $1'
	alias vin=' vim '
	alias vum=' vim '
	alias vun=' vim '
	alias vvim=' vim '
	alias VDC=' vim docker-compose.yml '
	alias WKGAP=' watch kubectl get pod -A '
	alias WKGAS=' watch kubectl get svc -A '
	CBG() { curl  --connect-timeout 2 -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile |grep -i --color $1 2>/dev/null || cat ~/.bash_profile 2>/dev/null |grep -i --color $1 ; }
	WATCH() { watch "$@" ; }
	DIG() { echo "dig $1 |grep -vE ^\;\|^$" ; dig $1 | grep -vE ^\;\|^$ ; }
	lgc() { cat $(ls -Ah |grep -i $1 ) || ls -Ah | grep -i $i ; }
	lgcd() { cd $(ls -Ah |grep -i $1 ) || ls -Ah | grep -i $i ; }
	lgv() { vim $(ls -Ah |grep -i $1 ) || ls -Ah | grep -i $i ; }
	KDKN() { kubectl $1 $2 $3 -n $(kubectl get $2 -A | grep $3 | awk "{print\$1}") ; }
	NUP() { useradd $1 -ms/bin/bash && echo "$1:$1" | chpasswd ; } # echo $1 | passwd --stdin $1
	mkdirc() { mkdir $1 ; cd $1 ; }
	nss2() { ssh $1 sudo docker ps | wc ; }
	WHOIS() { whois $1 | grep -Ev "^\s*(#|$)" |grep -i NetName ; }
	AWK() { awk $2 "{print\$$1}" ; }
	ECDZ() { echo 'source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"' |tee ~/.bash_profile /home/e/.bash_profile /home/ubuntu/.bash_profile ; }
	BRC() { curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile |grep -vEi raw.githubusercontent.com\|git_ps1 | ssh $1 "cat > .BRC" ; }
	NSP() { nmap -T5 -snP $1 |G report | AWK 5 ; }
	DF() { df -h |sort -hk2 |sed -e 1b -e '$!d' ; }
	SEDID() { sed -i "/$1/d" $2 ; }
	SEDIS() { sed -i "s/$1/$2/g" $3 ; }
	nss() { ssh -i ~/.ssh/nss2.pem ubuntu@$1 ; } 
	nsscp() { scp -ri ~/.ssh/nss2.pem ubuntu@$1 ; }
	NTPL() { growpart /dev/sda 2 ; resize2fs /dev/sda2 ; hostnamectl set-hostname u$(echo $1 |awk -F. "{print\$4}") ; sed -i "s/\(- \).*\(\/\)/\1$1\2/g" /etc/netplan/00-installer-config.yaml ; }
	sl() { for SOMETHING in $(cat ./$1); do $2 $SOMETHING; done; } # add "" to make $2 flaxable
	CMPL() { $1 completion bash > /etc/bash_completion.d/$1 || ls -l /etc/bash_completion.d ; }
	DOCKERTP() { sudo docker tag $1 ernie18a/$2:$3 ; sudo docker push ernie18a/$2:$3 ; }
	KEI() { kubectl exec -it $1 -- $2 2>/dev/null || kubectl -n kube-system exec -it $1 -- $2 2>/dev/null ; }
	PING() { for i in $(seq 0 255) ; do ping $1.$i -c1 -W1 & done |grep from |awk '{print $4}' |awk -F: '{print $1}' | sort -V ; }
	FFMPEG1() { for i in *.$1 ; do ffmpeg -y -i "$i" "$(echo "$i"|cut -d\. -f1).$2" & done ; }
	FFMPEG() { for i in *.$1 ; do ffmpeg -y -i "$i" "$(echo "$i"|cut -d\. -f1).$2" ; done ; }
	HI() { helm install $(echo -n $1 |AWK 2 -F/) $1 -n $2 2>/dev/null || helm install $(echo -n $1 |AWK 2 -F/) $1 ; }
	INGRESS() { helm install ingress-nginx ingress-nginx/ingress-nginx -n kube-system ; kubectl patch IngressClass nginx -p '{"metadata": {"annotations":{"ingressclass.kubernetes.io/is-default-class":"true"}}}' ; }
	KLPN() { kubectl logs $1 -n $(kubectl get po -A | grep $1 | awk "{print\$1}") ; }
	HLA() { helm ls -A |awk '{print $2"\t"$1}' | column -t || echo apt\ install\ -y\ bsdmainutils ; }
	YTDLV() { while read -r i; do yt-dlp -c -r 30k -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' "$i"; done < "$1"; }
	YTDLA() { while read i ; do yt-dlp -c -r 30k --prefer-ffmpeg --extract-audio --audio-format mp3 --no-check-certificate "$i" ; done <$1 ; }
	PUSHM() { git rm -r --cached . > /dev/null ; git add -A ; git commit -m "$@" &>/dev/null ; git push;}
	HSRG() { helm search repo $1 |grep -i --color $1 ; }
	GRCA() { cd ~/.G ; for REPO in `ls`; do (cd "$REPO"; git fetch --prune ; git reset --hard && git clean -f -d ; git rm -r . --cached . > /dev/null ; git reset --hard ; git pull ); done; cd - ; }
	CNG() { cat ~/.G/misc/notes/commands.txt 2>/dev/null |grep -i --color $1 || curl -fsSL https://gitlab.com/ernie18a/misc/-/raw/main/notes/commands.txt |grep -i --color $1 ; }
	CEG() { cat ~/.G/misc/notes/eng.txt 2>/dev/null |grep -i --color $1 || curl -fsSL https://gitlab.com/ernie18a/misc/-/raw/main/notes/eng.txt |grep -i --color $1 ; }
	CT() { cat ~/.T$1 2>/dev/null ; }
	VT() { vim ~/.T$1 ; }
	HG() { cat ~/.bash_history |grep -ai --color "$1" ; }
	CB() { cat ~/.bash_profile 2>/dev/null || curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile ; }
