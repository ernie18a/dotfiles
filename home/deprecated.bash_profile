#       alias BRCL=' echo source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)" > ~/.BRC '
#       BRCL() { curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile |grep -vEi raw.githubusercontent.com\|git_ps1 | ssh $1 "cat > .BRC" ; }
#       alias CKAR=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/.deprecated/i.kube.apt.reset.sh | bash '
#       alias KREW=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.krew.sh | bash '
#       alias KUS=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.kustomize.sh | bash '
#       alias FP=' find "$(pwd)" '
#       CGC() { grep -iR --color $1 ~/.G/dotfiles/curl ; }
#       CGM() { grep -iR --color $1 ~/.G/misc/deploying ; }
#       NTPL() { growpart /dev/sda 2 ; resize2fs /dev/sda2 ; sed -i "/\//d" /etc/netplan/00-installer-config.yaml ; sed -i "/gateway4/i \ \ \ \ \ \ - $1/24" /etc/netplan/00-installer-config.yaml ; hostnamectl set-hostname u$(echo $1 |awk -F. "{print\$4}") ; }
#       HI() { helm install $(echo -n $1 |AWK 2 -F/) $1 -n $2 || helm install $(echo -n $1 |AWK 2 -F/) $1 ; }
#       YTDLV() { while read i ; do yt-dlp --no-check-certificate --write-auto-sub --sub-lang en "$i" ; done <$1 ; }
#       YTDLV() { while read i ; do yt-dlp --no-check-certificate --write-auto-sub --sub-lang en --merge-output-format mp4 "$i" ; done <$1 ; }
#       BRC() { ssh-copy-id $i && (cat ~/.ssh/authorized_keys | grep -qE "e@mac|123@tgfc" || cat ~/.ssh/id_rsa.pub | ssh $i 'cat >> ~/.ssh/authorized_keys') && curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile | grep -vEi raw.githubusercontent.com\|git_ps1 | ssh $i "cat > .BRC" ; }
#       export PS1="[\\h \w \$(__git_ps1 '%s')] \\$ "
#       export PS1="[\\h \w \$(git rev-parse --abbrev-ref HEAD 2>/dev/null)] \\$ "
#       source /dev/stdin <<< "$(curl -fsSL https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh)" # change it to source local to bypass network unreachable

#       alias sl=' ls '
#       alias LTH=' sed -i "/help\|/mtr\|gping\|BRC\|wget\|NC\|ping\|curl\|HG\|HH\|LTH\|STEALTH\|source\|SAS\|export\|history\|rm\|ernie18a\|sshrc\|rsync\|scp\|scpr/d" ~/.bash_history ; rm -rf /tmp/*sshrc* '
#       alias YSDL=' sudo yum --showduplicates list ' # $1
#       lgc() { ls -Ah |grep -i $1 |xargs cat ; }
#       lgcd() { ls -Ah |grep -i $1 |xargs cd ; }
#
#	AWK() { awk "{print\$$1}" ; }
#	BB() { kubectl delete pod ernie-busybox 2>/dev/null ; kubectl run -it ernie-busybox --image=busybox --rm -- $1 ; }
#	CBG() { cat ~/.bash_profile 2>/dev/null |grep -i --color $1 || curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile |grep -i --color $1 ; }
#	CNG() { cat ~/.G/misc/notes/commands.txt 2>/dev/null |grep -i --color $1 || curl -fsSL https://gitlab.com/ernie18a/misc/-/raw/master/notes/commands.txt |grep -i --color $1 ; }
#	CURL() { while true ; do curl -sS $1?$(date +%s) ; echo -n $(date +%H:%M:%S.%N)\ ; done ; }
#	DLI() { docker login -u $(cat .docker/config.json |G auth.*== |AWK 2 |tr '"' ' ' |AWK 1 |base64 -d |AWK 1 -F:) -p $(cat .docker/config.json |G auth.*== |AWK 2 |tr '"' ' ' |AWK 1 |base64 -d |AWK 2 -F:) }
#	DOCKERBTP() { docker build . -t $2 ; docker tag $2 ernie18a/$1:$2 ; docker push ernie18a/$1:$2 ; }
#	DOCKERTP() { docker tag $2 ernie18a/$1:$2 ; docker push ernie18a/$1:$2 ; }
#	DRUN() { docker run -h $1 --name $1 -itd $1 ; } # might be failed if $1 is abc.io/nginx
#	ECDZ() { echo ' source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)" ' > ~/.bash_profile ; }
#	GDO() { curl -s https://api.github.com/repos/$1/$2/releases/latest |grep browser_download_url |grep linux.$(uname -m)\" |awk "{print\$2}" |xargs wget -O /bin/$3 ; chmod 0755 /bin/$3 ; }
#	GRSED() { grep -lR $1 . | xargs sed $3 "s/$1/$2/g" ; }
#	HI() { a=$(echo -n $1 |awk -F/ "{print\$2}") ; helm install $a $1 ; } # cannot apply ns?
#	HLA() { helm ls -A |awk '{print $1"\t"$2}' ; }
#	HRM() { sed -i "/$1/d" ~/.bash_history ; }
#	KD() { kubectl describe $2 -n $1 2>/dev/null || kubectl describe $1 2>/dev/null ; }
#	KEI() { kubectl exec -it $1 -- $2 2>/dev/null ; }
#	KGAG() { kubectl get -A $1 | grep -i --color $2 ; }
#	KGPC() { kubectl get pods $1 -o jsonpath="{.spec.containers[*].name}" 2>/dev/null | tr " " "\n" ; }
#	KGR() { kubectl get rc,$1 ; }
#	KL() { kubectl logs $1 -n $2 --all-containers 2>/dev/null || kubectl logs $1 --all-containers ; }
#	KL() { kubectl logs $1 -n $2 --all-containers || kubectl logs $1 --all-containers ; }
#	KL() { kubectl logs $2 -n $1 2>/dev/null || kubectl logs $1 2>/dev/null ; }
#	NTPL.ORIGINAL() { growpart /dev/sda 2 ; resize2fs /dev/sda2 ; sed -i "/\//d" /etc/netplan/00-installer-config.yaml ; sed -i "/gateway4/i \ \ \ \ \ \ - $1/24" /etc/netplan/00-installer-config.yaml ; hostnamectl set-hostname u$(echo $1 |awk -F. "{print\$4}") ; }
#	SUL() { sudo updatedb ; sudo locate -i $1 | grep -i --color $1 ; }
#	YTDL1() { while read i ; do yt-dlp --prefer-ffmpeg --extract-audio --audio-format mp3 --no-check-certificate "$i" ; done <$1 ; }
#	YTDLXI() { while read i ; do yt-dlp -xi --no-check-certificate "$i" ; done <$1 ; }
#	alias ALPINE=' docker build -t alpine:ernie https://gitlab.com/ernie18a/misc/-/raw/master/deploying/Dockerfile.alpine 1>/dev/null ; echo -e "kubectl run --image=alpine:ernie alpine\nkubectl exec -it alpine -- bash\ndocker run -h alpine --name alpine -id alpine:ernie\ndocker exec -it alpine bash" '
#	alias AMPERE=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.ampere.sh | bash '
#	alias AUTH=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.authorizing.sh | bash '
#	alias AWX=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.awx-operator.sh | bash '
#	alias C8=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.c8.sh | bash '
#	alias CALICO=' kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml '
#	alias CDG=' cd ~/.G || mkdir ~/.G ; cd ~/.G ; ls -A --color |sort '
#	alias CDG=' cd ~/.G/* 2>/dev/null || cd ~/.G '
#	alias CDG=' cd ~/.G/* 2>/dev/null || mkdir ~/.G 2>/dev/null ; cd ~/.G ; ls -A --color '
#	alias CDG=' cd ~/.G/* || cd ~/.G ; ls -A --color |sort '
#	alias CDG=' cd ~/.G/* || mkdir ~/.G ; cd ~/.G ; ls -A --color |sort '
#	alias CDWV=' cd $WIN_USER_DIR/vagrant '
#	alias CIY=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.yum.sh | bash '
#	alias CKYM=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.yum.m.sh | bash '
#	alias CKYR=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.yum.reset.sh | bash '
#	alias CNG=' cat ~/.G/misc/notes/commands.txt | grep --color -i '
#	alias CV=' cat values.yaml '
#	alias DAE=' ls -a ~ |grep -v "ssh$\|.kube$\|.vimrc$" |xargs rm -rf '
#	alias DCD=' docker-compose down '
#	alias DCL=' docker ps -a --format {{.Names}} |xargs docker inspect |grep -i --color Source '
#	alias DCLF=' docker-compose logs -f '
#	alias DCP=' docker-compose ps '
#	alias DCUB=' docker-compose down && docker-compose up --build -d '
#	alias DIA=' for i in $(DP |AWK 1) ; do docker inspect $i 2>/dev/null |grep IPAddress.*172 ; done '
#	alias DLF=' docker logs -f '
#	alias DLN=' docker logs -n111 '
#	alias DOCKERKILL=' docker kill $(docker ps -q) ; docker rm -f $(docker ps -a -q) ; docker rmi -f $(docker images -q) ; docker rm -v $(docker ps -a -q -f status=exited) ; docker rm -v $(docker ps -a -q -f status=created) '
#	alias DP2=' docker ps -a --format "table {{.Names}}\t{{.Status}}\t{{.Ports}} " || podman ps -a --format "table {{.Names}}\t{{.Status}}\t{{.Ports}} " '
#	alias DPK=' docker ps -a --format "{{.Names}}" | awk -F_ "{print\$3\"\ \@\ \"\$4}" 2>/dev/null '
#	alias DR=' docker restart '
#	alias DRA=' for i in $(docker ps -a --format "{{.Names}}") ; do docker restart $i ; done '
#	alias DRF=' docker rm -f '
#	alias DRLF=' docker-compose down ; docker-compose up -d ;docker-compose logs -f '
#	alias DS=' docker stop '
#	alias EXIT=' docker rm $(docker ps -a -f status=exited -q) '
#	alias GACM=' git add -A ; git commit -m '
#	alias GCB=' git checkout -b '
#	alias GD=' git --no-pager diff '
#	alias GPP=' git pull ; git rm -r --cached . > /dev/null ; git add -A ; git commit -amxxxxx > /dev/null ; git push '
#	alias INGDEMO=' curl -fsSL https://gitlab.com/ernie18a/misc/-/raw/master/scripts/ing.demo.sh | bash '
#	alias INGRESS=' kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.4/deploy/static/provider/baremetal/deploy.yaml '
#	alias INGRESS=' kubectl create ns ingress-nginx ; helm install ingress-nginx bitnami/nginx-ingress-controller -n ingress-nginx '
#	alias IPA=' ip a |grep -E ens\|eth\|wlan0 |grep -iE --color "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | awk "{print\$2}"| awk -F/ "{print\$1}" '
#	alias KCX2=' kubectl run --image nginx --port=80 nginx ; kubectl expose pod nginx '
#	alias KCX=' kubectl create deployment nginx-ernie18a --image=ernie18a/nginx:v1 ; kubectl scale --replicas=2 deployment/nginx-ernie18a ; kubectl expose deployment nginx-ernie18a --type=LoadBalancer --name=nginx-ernie18a --port=80 ; kubectl get svc '
#	alias KDC=' kubectl delete pod --field-selector=status.phase==Succeeded ; echo --all-namespaces ' # cannot define specfc ns
#	alias KDELO=' kubectl get all |grep -v ^NAME | grep -v service\/kubernetes | awk "{print\$1}" | xargs kubectl delete --force --grace-period=0 '
#	alias KDF=' kubectl delete -f '
#	alias KDF=' kubectl delete pod -A --field-selector=status.phase==Failed '
#	alias KDN=' kubectl describe nodes '
#	alias KDP=' kubectl describe pod '
#	alias KDX2=' kubectl delete po nginx ; kubectl delete svc nginx '
#	alias KGAA=' kubectl get all -A '
#	alias KGAPFNR=' kubectl get pods --field-selector status.phase!=Running -A '
#	alias KGAS=' kubectl get -A svc |grep -v nfs-nfs-server-provisioner '
#	alias KGASW=' kubectl get svc -A -o wide -o custom-columns="NAMESPACE:{metadata.namespace},NAME:{metadata.name},TYPE:{spec.type},CLUSTER-IP:{spec.clusterIP},EXTERNAL-IP:{.status.loadBalancer.ingress[*].ip},PORD:{.spec.ports[*].port}" '
#	alias KGES=' kubectl get event -A -o custom-columns="NAME:{metadata.name},MESSAGE:{message}" '
#	alias KGY=' kubectl get -o yaml'
#	alias KL=' kubectl logs --all-containers '
#	alias LONGHORN=' kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/master/deploy/longhorn.yaml '
#	alias LTH=' sed -i "/mtr\|gping\|BRC\|wget\|NC\|ping\|curl\|HG\|HH\|LTH\|STEALTH\|source\|SAS\|export\|history\|rm\|ernie18a\|sshrc\|rsync\|scp\|scpr/Id" ~/.bash_history ; rm -rf /tmp/*sshrc* '
#	alias M24=' for i in *.webm;do ffmpeg -y -i "$i" "$(echo "$i"|cut -d\. -f1).mp4"; done '
#	alias M2G=' for i in *.webm;do ffmpeg -y -i "$i" "$(echo "$i"|cut -d\. -f1).gif"; done '
#	alias MASTER=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.master.sh | bash '
#	alias MC=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.minecraft.sh | bash '
#	alias METRICS=' kubectl apply -f https://gitlab.com/ernie18a/dotfiles/-/raw/main/misc/metrics-server.yaml '
#	alias MINIO=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.kube.minio.sh | bash '
#	alias NVIM=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.nvim.sh | bash '
#	alias ORACLE=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.oracle.sh | bash '
#	alias P2P=' for i in *.webp;do ffmpeg -y -i "$i" "$(echo "$i"|cut -d\. -f1).png"; done '
#	alias PIU=' pip3 install -U '
#	alias PIU=' umask 022 ; sudo pip3 install -U '
#	alias PRUNE=' docker system prune -fa 2>/dev/null ; docker system prune -f --volumes 2>/dev/null '
#	alias PUSH=' git rm -r --cached . > /dev/null ; git add -A ; git commit -amxxxxxxx > /dev/null ; git push '
#	alias RMH=' sed -i "/$1/d" ~/.bash_history '
#	alias SS=' ss -nlp |V 127.0.0 |GE ":\*" |column -t '
#	alias SS=' ss -nlp |V 127.0.0 |GE ":\*" |column -t 2>/dev/null || ss -nlp |V 127.0.0 |GE ":\*" '
#	alias SS=' ss -ntlp '
#	alias SS=' ss -ntlp |grep -v 127.0.0.1 '
#	alias SS=' ss -pln |V \ 127\. |GE ^tcp\|^udp | column -t 2>/dev/null || ss -pln |V \ 127\. |GE ^tcp\|^udp '
#	alias SS=' ss -pln |VE 127.0.\|^u_ |grep LISTEN | column -t 2>/dev/null || ss -pln |grep -v LISTEN |VE 127.0.\|^u_ '
#	alias SSG=' ss -pln |V \ 127\. |GE ^tcp\|^udp |grep -i --color '
#	alias SU=' echo "source /tmp/.e.sshrc.*/.sshrc" ; echo "source /home/$(whoami)/.bash_profile " ; sudo su - '
#	alias SUS=' | sort | uniq -c | sort -nk2 '
#	alias TERRAFORM=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.terraform.apt.sh | bash '
#	alias TMP=' sudo rm -rf /tmp/{.,}* || rm -rf /tmp/{.,}* '
#	alias TnS=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.transmission.N.shadowsocks-libev.sh | bash '
#	alias VDF=' vagrant destroy -f '
#	alias VIMRC=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.vimrc >> ~/.vimrc '
#	alias VRP=' git pull && vim README.md && git rm -r --cached . > /dev/null && git add -A && git commit -amREADME.md\ update &> /dev/null && git push ' # DEPRECATED
#	alias VS=' vagrant ssh '
#	alias VU=' vagrant up ' # --provider hyperv
#	alias VVF=' vim Vagrantfile '
#	alias WKGAPFNR=' watch "kubectl get po -A |grep -v Running" '
#	alias WSL=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.wsl.sh | bash '
#	alias WV=' /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe '
#	alias WVDF=' /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe destroy -f '
#	alias WVS=' /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe ssh '
#	alias WVU=' /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe up '
#	alias WVUS2=' ls Vagrantfile &>/dev/null || cd $WIN_USER_DIR/vagrant ; /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe status |grep running && /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe ssh || /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe up ; /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe ssh '
#	alias WVUS=' ls Vagrantfile &>/dev/null || cd $WIN_USER_DIR/vagrant ; /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe up ; /mnt/c/HashiCorp/Vagrant/bin/vagrant.exe ssh '
#	alias bcr=' bat -ppl rb '
#	alias cp=' cp -urf '
#	alias lgc=' lg bat | xargs cat '
#	alias lgc=' ls -Ah |grep -i |xargs cat '
#	alias tmprm=' sudo rm -rf /tmp/{,.}* 2>/dev/null ; ls -A /tmp && echo /tmp\ is\ now\ empty '
#	eval $(ssh-agent) ; ssh-add ~/.ssh/id_rsa
#	export DEBIAN_FRONTEND=noninteractive
#	export HISTTIMEFORMAT="%F %T " &>/dev/null
#	export LANG=en_US.UTF-8 &>/dev/null
#	export LANGUAGE=en_US.UTF-8 &>/dev/null
#	export LC_ALL=en_US.UTF-8 &>/dev/null
#	export PATH="/bin:/sbin:/usr/bin:/usr/games:/usr/local/bin:/usr/local/sbin:/usr/sbin:~/.local/bin:~/.cargo/bin:${KREW_ROOT:-$HOME/.krew}/bin"
#	export PATH="/bin:/sbin:/usr/bin:/usr/games:/usr/local/bin:/usr/local/sbin:/usr/sbin:~/.local/bin:~/.cargo/bin:/root/.krew/bin"
#	export PATH="/bin:/sbin:/usr/bin:/usr/games:/usr/local/bin:/usr/local/sbin:/usr/sbin:~/.local/bin:~/.cargo/bin:~/.krew/bin"
#	export PS1="[\u@\h \w]\\$ \[$(tput sgr0)\]"
#	export VAGRANT_DISABLE_VBOXSYMLINKCREATE=1
#	gvbc() { grep -v ^\ *$ |grep -v ^\ *# |grep -v ^\t*$ |grep -v ^\t*# |grep -v ^$'\r' | grep -vP ^'\t*#'| bat -ppl rb ; }
#	gvbcl() { grep -v ^\ *$ |grep -v ^\ *# |grep -v ^\t*$ |grep -v ^\t*# |grep -v ^$'\r' | grep -vP ^'\t*#'| bat -ppl $1 $2 ; }
#	keychain --nogui ~/.ssh/id_rsa ; source ~/.keychain/$(hostname)-sh
#	lgcd() { ls -Ah |grep -i $1 |xargs cd ; }
#	llgm() {if [[ $(ls |grep -i $1 |wc -l) = 1 ]] ; then mv *$1* ../_p*/_a ; fi }
# 	alias INGRESS=' kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.0/deploy/static/provider/cloud/deploy.yaml '
# function
#	export CRIT_CONFIG=/etc/crictl.yaml
#	export CONTAINER_RUNTIME_ENDPOINT=/run/containerd/containerd.sock
#	export IMAGE_SERVICE_ENDPOINT=/run/containerd/containerd.sock
#	export PATH="/bin:/sbin:/usr/bin:/usr/games:/usr/local/bin:/usr/local/sbin:/usr/sbin:/usr/sbin:~/.local/bin:~/.cargo/bin:$HOME/.krew/bin:/snap/bin"
#	export PATH="$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.krew/bin:$GOPATH/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/snap/bin"
#	export VMWARE_VALIDATE_CERTS=no
#	alias NE=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.node.exporter.sh | bash '
#	alias TRANS=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.transmission.sh | bash '
#	NTPL() { growpart /dev/sda 2 ; resize2fs /dev/sda2 ; hostnamectl set-hostname u$(echo $1 |awk -F. "{print\$4}") ; echo /etc/netplan/00-installer-config.yaml ; }
#	YTDLV() { while read i ; do yt-dlp -c -r 1k https://aiguides.com/advanced-chatgpt-prompts/  -f bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best "$i" ; done <$1 ; }
#	alias DU=' du -sh 2>/dev/null ; du -hd1 2>/dev/null |grep "G\|M" '
#	alias CDT=' cd /tmp ; ls -Al --color | sort '
#	KGPN() { kubectl get po $1 -n $(kubectl get po -A | grep $1 | awk "{print\$1}") ; }
#	alias AALH=' ansible all --list-hosts '
#	alias AP=' ansible-playbook '
#	alias APB=' ansible-playbook '
#	alias APH=' ansible-playbook -i .hosts '
#	alias COLUMN=' apt-get install -y bsdmainutils || yum install -y util-linux '
#	alias OVT=' apt-get update 2>/dev/null ; apt-get install -y open-vm-tools 2>/dev/null ||yum install -y open-vm-tools 2>/dev/null ; systemctl restart vmtoolsd.service ; systemctl enable vmtoolsd.service'
#	alias CEH=' cat /etc/hosts '
#	alias HH=' cat ~/.bash_history '
#	alias AUTH=' cat ~/.ssh/authorized_keys '
#	alias ccd=' cd '
#	alias CDT=' cd /tmp ; ls -A --color |sort '
#	alias CDGP=' cd ~/.G/.awx-cathay-cicd 2>/dev/null || cd ~/.G/awx-cathay-cicd '
#	alias CDGM=' cd ~/.G/misc/deploying ; ls -Al --color | sort '
#	alias FSSL=' curl -fsSL '
#	alias CKR2=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/_kube.reset.sh |bash '
#	alias SINGLE2=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/_kube.single.sh | bash '
#	alias AKE=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.authorized_keys.ed25519.sh | bash '
#	alias AK=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.authorized_keys.sh | bash '
#	alias METAL=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.metallb.sh | bash '
#	alias MINI=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.minikube.sh | bash '
#	alias TMUX=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.tmux.sh | bash '
#	alias PUB=' curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.pub.sh | bash '
#	alias CONTAINERD=' curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.containerd.sh | bash '
#	alias WD=' date +%A '
#	alias DATE=' date +%Y-%m-%d\ %H:%M:%S '
#	alias DNF=' dnf --disablerepo '*' --enablerepo=extras swap centos-linux-repos centos-stream-repos ; dnf distro-sync '
#	alias SA=' eval $(ssh-agent) 1>/dev/null ; ssh-add ~/.ssh/id_rsa &>/dev/null '
#	alias SAS=' eval $(ssh-agent) 1>/dev/null ; ssh-add ~/.ssh/id_rsa &>/dev/null ; ssh '
#	alias GBA=' git branch -a '
#	alias PULL=' git pull '
#	alias V200=' grep -iv " 200 \| 200$" '
#	alias HUA=' helm list |grep -v ^NAME |awk "{print\$1}" |xargs helm uninstall '
#	alias KCL=' kubectl create deployment nginx-ernie18a --image=nginx:alpine-slim ; kubectl scale --replicas=2 deployment/nginx-ernie18a ; kubectl expose deployment nginx-ernie18a --type=LoadBalancer --name=nginx-ernie18a --port=80 ; kubectl get svc '
#	alias KDL=' kubectl get all |grep nginx-ernie18a |awk "{print\$1}" |xargs kubectl delete '
#	alias KGASW=' kubectl get svc -A -o custom-columns="NAMESPACE:{metadata.namespace},NAME:{metadata.name},TYPE:{spec.type},CLUSTER-IP:{spec.clusterIP},EXTERNAL-IP:{.status.loadBalancer.ingress[*].ip},PORD:{.spec.ports[*].port}" '
#	alias lllc=' ls --color -Alh '
#	alias llc=' ls --color -l '
#	alias NN=' netstat -an -ptcp | grep LISTEN | V 127.0.0.1 | V ::1 '
#	alias SOURCE=' source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)" '
#	alias SCT=' ssh -o ConnectTimeout=300 '
#	alias dietpi=' ssh dietpi@0 -p20 '
#	alias u250=' ssh root@0 -p21 '
#	alias CCAT=' sudo curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/.sbin/ccat -o /bin/ccat ; sudo chmod +x /bin/ccat '
#	alias VEH=' sudo vim /etc/hosts || vim /etc/hosts '
#	alias WIDTH=' tput cols '
#	alias VGP=' vim .gitlab-ci.yml ; git rm -r --cached . > /dev/null ; git add -A ; git commit -amxxxxxx &> /dev/null ; git push '
#	FPG() { find "$(pwd)" | grep -i --color $1 ; }
#	UA() { useradd $1 -ms/bin/bash ; echo -e "$2\n$2" | passwd $1 ; }
#	GV2() { grep -PEv ^\ *$\|^\ *#\|^\t*$\|^\t*#\|^$'\r'\|^'\t*#' ; }
#	GV3() { grep -v '^[[:space:]]*$\|^[[:space:]]*#' | grep -v '^[[:space:]]*$' | grep -v $'\r' ; }
#	CMPL() { ls /etc/bash_completion.d && $1 completion bash > /etc/bash_completion.d/$1 ; }
#	KIN() { kubectl exec $1 -- bash -c "apt update ; apt install -y dnsutils watch curl iputils-ping iproute2 nmap vim lsof apt-file telnet ; curl -fsSl https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile -o ~/.bashrc " ; }
#	alias 2ALL=' M2G ; M24 ; P2P ; rm -rf *webm *webp '
#	alias HUSH=' touch ~/.hushlogin '
#	GUA() { for REPO in `ls`; do (cd "$REPO"; git fetch --prune ; git reset --hard && git clean -f -d ; git rm -r --cached . > /dev/null ; git reset --hard ; git pull ); done; }
#	touch ~/.hushlogin # disabled due to 15u9312
#	alias B6=' echo "$1" | base64 -d '
#	alias SN='
#	alias DV=' grep -vE ^\;\|^$ '
#	alias HG=' cat ~/.bash_history |grep -ai --color '
#	TEST4() { bash -c "curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.apt.sh | bash -s \"$@\"" ; } # https://gemini.google.com/app/541a8f0e276347f3
#	alias APT=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.apt.sh | bash '
#	alias MINI=' curl -fssL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.minimum.sh | bash '
#	alias IPP=' curl ifconfig.co; curl ifconfig.io '
#	alias GRI=' grep -ri --color 2>/dev/null ' # -i # didnt search ln
#	alias GURL=' grep -vE index.php\|index.htm\|log.*log\|127.0.0\|1.2.3 | grep -Eo "([a-zA-Z0-9]+\.[a-zA-Z0-9]+\.[a-zA-Z0-9]+)" ' # without -r
#	alias GURL=' grep -E "([a-zA-Z0-9]+\.[a-zA-Z0-9]+\.[a-zA-Z0-9]+)" ' # -r & -o removed
#	alias 8888=' ping 8.8.8.8 '
#	GV() { grep -v ^\ *$ |grep -v ^\ *# |grep -v ^\t*$ |grep -v ^\t*# |grep -v ^$'\r' | grep -vP ^'\t*#' ; }
#	GV2() { grep -Pv '^\s*$|^\s*#|^\t*$|^\t*#|^$'\r'|^\t*#' ; }
#	GV() { grep -Ev '^\s*(#|$)' ; }
#	source ~/.inputrc
#	alias nss=' ssh -i ~/.ssh/nss2.pem '
#	alias sed=' gsed 2>/dev/null || sed '
#	alias GRC=' git fetch --prune ; git reset --hard && git clean -f -d ; git rm -r --cached . > /dev/null ; git reset --hard ; git pull '
#	alias SSG=' ss -pln |GE ^tcp\|^udp |grep -i --color '
#	alias SS=' ss -pln |GE ^tcp\|^udp\|Address:Port | column -t 2>/dev/null || ss -pln |GE ^tcp\|^udp '
#	GV() { grep -Ev "^\s*(#|$)" ; }
#	CBG() { curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile |grep -i --color $1 2>/dev/null || cat ~/.bash_profile 2>/dev/null |grep -i --color $1 ; }
