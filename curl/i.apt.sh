#!/bin/bash
	timedatectl set-timezone Asia/Taipei
#	rm -rf ~/.* /home/e/.* /home/ubuntu/.*
#	apt-get purge -y --allow-remove-essential needrestart iptables* ufw* apparmor* firewall* git* unattended-upgrades &>/dev/null
	apt-get purge -y ufw* apparmor* needrestart unattended-upgrades &>/dev/null
	apt-get update ; apt-get install -yq rsync apt-utils bash-completion tmux vim bsdmainutils apt-transport-https
	echo ServerAliveInterval\ 30 >> /etc/ssh/ssh_config
	echo StrictHostKeyChecking\ no >> /etc/ssh/ssh_config
	echo TCPKeepAlive\ no >> /etc/ssh/ssh_config
	echo ForwardAgent\ yes >> /etc/ssh/ssh_config
	echo ClientAliveInterval\ 60 >> /etc/ssh/sshd_config &>/dev/null
	echo ClientAliveCountMax\ 3 >> /etc/ssh/sshd_config &>/dev/null
	swapoff -a ; sed -i '/swap/ s/^/#/' /etc/fstab
#	echo "e ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
#	echo "ubuntu ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
	curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/e.brc.n.auth.sh | bash
	echo 'source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"' |tee ~/.bash_profile /home/e/.bash_profile /home/ubuntu/.bash_profile 1>/dev/null
case "$1" in
  wsl)
	apt-get update ; apt-get install -yq dos2unix file jq software-properties-common tree unzip autossh # wsl
	add-apt-repository ppa:git-core/ppa
	apt-get update ; apt-get install -yq git
	apt-get autoremove -y ; dpkg -l | grep "^rc" | awk "{print\$2}" | xargs apt-get purge -y
	export WIN_USER=$(ls /mnt/c/Users 2>/dev/null |grep -iv "All\|Default\|desktop.ini\|Public\|USER\|Administrator\|super")
	export WIN_USER_DIR=/mnt/c/Users/$WIN_USER &>/dev/null
	export WIN_USER_DL=/mnt/c/Users/$WIN_USER/Downloads
	unzip $WIN_USER_DL/ernie-master.zip
#	unzip $WIN_USER_DL/55h-main.zip
	timedatectl set-timezone Asia/Taipei

	# /root/.ssh
	mkdir ~/.ssh
	cat ernie-master/.ssh/id_rsa > ~/.ssh/id_rsa
	cat ernie-master/.ssh/id_rsa.pub > ~/.ssh/id_rsa.pub
	chmod 0700 ~/.ssh
	chmod 0600 ~/.ssh/id_rsa
	chmod 0644 ~/.ssh/id_rsa.pub
	e/.ssh
	cp -rf ~/.ssh /home/e/.ssh
	chmod 0700 /home/e/.ssh
	chmod 0600 /home/e/.ssh/id_rsa
	chmod 0644 /home/e/.ssh/id_rsa.pub
	/home/e/
	mkdir /home/e/.G/
	cd /home/e/.G/
	git clone git@gitlab.com:ernie18a/dotfiles.git
	git clone git@gitlab.com:ernie18a/misc.git
	git clone https://github.com/tmux-plugins/tpm /home/e/.G/.tmux_plugins_manager      
	sed -i 's/https:\/\/gitlab.com\/ernie18a\/dotfiles.git/git@gitlab.com:ernie18a\/dotfiles.git/g' /home/e/.G/dotfiles/.git/config
	ln -snf /home/e/.G/dotfiles/home/.bash_profile /home/e/.bash_profile
	ln -snf /home/e/.G/dotfiles/home/.gitconfig /home/e/.gitconfig
	ln -snf /home/e/.G/dotfiles/home/.tmux.conf /home/e/.tmux.conf
	ln -snf /home/e/.G/dotfiles/home/.vimrc /home/e/.vimrc
	chown -R e:e /home/e
	cat /home/e/.G/dotfiles/home/.hyper.js > $WIN_USER_DIR/AppData/Roaming/Hyper/.hyper.js 2>/dev/null
    ;;
  oci)
    # Replace this comment with the actual commands to be executed for Ampere
    ;;
  vm)
    # Replace this comment with the actual commands to be executed for VM
#	sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
#       apt-get update ; apt-get install -yq dos2unix file jq software-properties-common tree unzip autossh # wsl
        apt-get update ; apt-get install -yq file jq software-properties-common tree
#       add-apt-repository ppa:git-core/ppa
#       apt-get update ; apt-get install -yq git open-vm-tools
        apt-get autoremove -y ; dpkg -l | grep "^rc" | awk "{print\$2}" | xargs apt-get purge -y
    ;;
  *)
    # If an unknown argument is passed, print an error message and exit
    echo "Usage: run.sh [wsl|ampere|vm]"
    exit 1
    ;;
esac
# endding
#	cp -rf ~/.ssh /home/e/.ssh
#	cp -rf ~/.ssh /home/ubuntu/.ssh
	chown -R e:e /home/e ; chown -R ubuntu:ubuntu /home/ubuntu
