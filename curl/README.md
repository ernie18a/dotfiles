[[_TOC_]]
# 使用方式
```bash
source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"
```
- 透過`curl ... | bash`執行放在gitlab上的bash script
- 以`i.runner.sh`為例：加入後`alias RUNNER=' curl -fsSL https://gitlab.com/ernie18a/dotfiles/-/raw/main/curl/i.runner.sh | bash '`，再透過指令`RUNNER`，安裝gitlab-ci runner。
- 命名原則
  - e*： 設定環境
  - i*： 安裝
# 整理 ~/.bash_profile
`e.new_bash_profile.sh`
- 保留常用組合
- 把已備註的組合移到`DEPRECATED.bash_profile`
- 把新`~/.bash_profile`推到gitlab上
# VM初始化
`i.apt.sh`
- 透過`case`選擇要初始化的環境。
- 目前有wsl、vm和default三種選項
### default
- 設定 ssh
- 設定 sudo 
#### wsl
- 設定windows user相關
- ssh private key
- pull常用工具
  - github.com/tmux-plugins/tpm
- tmux config
#### vm
- open-vm-tools
# 安裝helm
`i.helm.sh`
- 安裝helm
- `helm repo add*`
- `helm completion bash `
- `helm repo update`
# 安裝k8s的前置作業
`i.kube.reset.sh`
- 超強防呆機制
- `apt purge ...`
- `rm -rf ...`
- 設定 ipvs
- 設定 kernel module
- 執行`i.docker.sh`並安裝 cri-docker
- 安裝 k8s packages
- systemctl restart & enable
# 安裝單節點k8s
`i.kube.single.sh`
- `kubeadm init ...`
- 安裝cni
- 執行`i.helm.sh`
- 安裝metrics-server
- `kubectl taint ...`
- 安裝和設定 storageclass
# 安裝地端k8s loadbalancer
`i.metallb.sh`
- 需手指定ip pool
# 建立rhel dvd repo
`i.rhel.repo.mount.sh`
- 加dvd repo
- OS初始化
# 安裝gitlab runner
`i.runner.sh`
- 超強防呆機制
- 設定權限
# 裝docker
`i.docker.sh`
- `.... get.docker.com ...`
- `usermod ...`
- `systemctl ...`
- `docker completion bash > ...`
- 安裝最新docker-compose
# 其他
### 安裝bit torrent
`i.transmission.sh`
### 安裝socks5 proxy
`i.shadowsocks.sh`
### 加入測試專用 public & private ssh key 
`e.authorized_keys.ed25519.sh`
### 加入私人 public ssh key
`e.authorized_keys.sh`
### 在陌生環境暫時使用私人~/.bash_profile
`e.danger.zone.sh`
- `source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"`
### 安裝golang
`i.golang.sh`
- `add-apt-repository ppa:longsleep/golang-backports`
### 安裝node exporter
`i.node.exporter.sh`
### 安裝 nodejs npm
`i.npm.sh`
### 設定syntax highlight
`i.bat.sh`
### 加入常用`~/.vimrc`
`e.vimrc.sh`
