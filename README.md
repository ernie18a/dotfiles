[[_TOC_]]
# ./curl/
- 搭配 `~/.bash_profile` 中 `curl ... | bash` 系列的 alias 可在任何 vm 上執行腳本
# ./home/
- 多個 dotfile ，透過 `./curl/i.apt.sh` ，在家目錄下建立連結(`ln -s`)
# 初始化
- 初始化後可使用 `~/.bash_profile` 內兩百多個 alias 和 bash function
```bash
source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"
```