[[_TOC_]]
- ask 41 to refer sshrc and create a thing for `source ..... /dev/stdin ......`
- add source brc at front of watch alies if there is one, 
- fix user's .config/helm from 1.helm.sh
  - user need to helm repo update to read it.
- remove some command from " git push " related command.
- add envsubst for metallb ip range
  - add envsubst to ~/.G/misc/deploy/kube.ing/ing.yaml for preciseness host name 
- nmap -sn  172.16.211.0/24
```sh
for i in $(seq 254); do ping 192.168.1.$i -c1 -W1 & done | grep from |AWK 4 |AWK 1 -F: |sort -V
```
